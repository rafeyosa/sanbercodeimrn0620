//Soal No. 1
var i = 0;
console.log("LOOPING PERTAMA");
while(i<20) {
	i+=2;
	console.log(i + " - I love coding");
}
console.log("LOOPING KEDUA");
while(i>0) {
	console.log(i + " - I will become a mobile developer");
	i-=2;
}

//Soal No. 2
for(var i = 1; i<=20; i++) {
	if(i%2 == 0) { //genap
		console.log(i + " - Berkualitas");
	} else if (i%2 == 1) { //ganjil
		if(i%3 != 0) {
			console.log(i + " - Santai");
		} else {
			console.log(i + " - I Love Coding");
		}
	}
}

//Soal No. 3
for(var i = 0; i<4; i++) {
	for(var j = 0; j<8; j++) {
		process.stdout.write("#");
	}
	process.stdout.write("\n");
}

//Soal No. 4
for(var i = 0; i<7; i++) {
	for(var j = 0; j<=i; j++) {
		process.stdout.write("#");
	}
	process.stdout.write("\n");
}

//Soal No. 5
for(var i = 0; i<8; i++) {
	for(var j = 0; j<8; j++) {
		if(((i%2 == 0) && (j%2 == 0)) || ((i%2 == 1) && (j%2 == 1))) {
			process.stdout.write(" ");
		} else {
			process.stdout.write("#");
		}
	}
	process.stdout.write("\n");
}