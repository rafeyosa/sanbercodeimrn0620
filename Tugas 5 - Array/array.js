//Soal No. 1
function range(startNum, finishNum) {
	var num = [];
	if(startNum == null||finishNum == null) {
		return -1;
	} else if(startNum<=finishNum) {
		for(var i = startNum ; i<=finishNum; i++) {
			num.push(i);
		}
		return num;
	} else if(startNum>=finishNum) {
		for(var i = startNum ; i>=finishNum; i--) {
			num.push(i);
		}
		return num;
	}
}
 
console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//Soal No. 2
function rangeWithStep(startNum, finishNum, step) {
	var num = [];
	if(startNum == null||finishNum == null) {
		return -1;
	} else {
		if(step == null) {
			step = 1;
		}

		if(startNum<=finishNum) {
			for(var i = startNum ; i<=finishNum; i+=step) {
				num.push(i);
			}
			return num;
		} else if(startNum>=finishNum) {
			for(var i = startNum ; i>=finishNum; i-=step) {
				num.push(i);
			}
			return num;
		}
	}
}
 
console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 

//Soal No. 3
function sum(startNum, finishNum, step) {
	if(finishNum == null) {
		finishNum = 1;
	}

	var sumNumArray = rangeWithStep(startNum, finishNum, step);

	var sumNum = 0;
	for(var i = 0 ; i<sumNumArray.length; i++) {
		sumNum = sumNum + sumNumArray[i];
	}
	return sumNum;

}

console.log(sum(1,10)) // 55
console.log(sum(5, 50, 2)) // 621
console.log(sum(15,10)) // 75
console.log(sum(20, 10, 2)) // 90
console.log(sum(1)) // 1
console.log(sum()) // 0 

//Soal No. 4
function dataHandling(input) {
	for(var i = 0; i<input.length; i++) {
		console.log("Nomor ID: "+input[i][0]);
		console.log("Nama Lengkap: "+input[i][1]);
		console.log("TTL: "+input[i][2]+" "+input[i][3]);
		console.log("Hobi: "+input[i][4]+"\n");
	}
}

//contoh input
var input = [
                ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
                ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
                ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
                ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
            ];

dataHandling(input);

//Soal No. 5
function balikKata(str) {
	var newStr = "";
	for(var i = str.length-1 ; i>=0; i--) {
		newStr+=str[i];
	}
	return newStr;
}
 
console.log(balikKata("Kasur Rusak")) // kasuR rusaK
console.log(balikKata("SanberCode")) // edoCrebnaS
console.log(balikKata("Haji Ijah")) // hajI ijaH
console.log(balikKata("racecar")) // racecar
console.log(balikKata("I am Sanbers")) // srebnaS ma I 

//Soal No. 6
function dataHandling2(data) {
	data.splice(1, 1, data[1]+" Elsharawy");
	data.splice(2, 1, "Provinsi "+data[2]);
	data.splice(4, 0, "Pria");
	data.splice(5, 1, "SMA Internasional Metro");
	console.log(data);

	var hasilSplitTanggal = data[3].split("/");

	console.log(getMonthName(hasilSplitTanggal[1]));

	var tanggal = data[3].split("/"); 
	// var tanggal = hasilSplitTanggal; merupakan dynamic memory, sehingga ketika data dalam variabel diubah, 
	//data pada variabel yang mereference juga ikut berubah
	tanggal.sort((a,b)=>b-a);
	console.log(tanggal);

	var hasilJoinTanggal = hasilSplitTanggal.join("-");
	console.log(hasilJoinTanggal);

	var sliceNama = data[1];
	sliceNama = sliceNama.toString();
	sliceNama = sliceNama.slice(0,15);
	console.log(sliceNama);
}

function getMonthName(bulan) {
	var intBulan = parseInt(bulan);
	var strBulan;
	switch(intBulan) {
	case 1:
		strBulan = "Januari";
		break;
	case 2:
		strBulan = "Februari";
		break;
	case 3:
		strBulan = "Maret";
		break;
	case 4:
		strBulan = "April";
		break;
	case 5:
		strBulan = "Mei";
		break;
	case 6:
		strBulan = "Juni";
		break;
	case 7:
		strBulan = "Juli";
		break;
	case 8:
		strBulan = "Agustus";
		break;
	case 9:
		strBulan = "September";
		break;
	case 10:
		strBulan = "Oktober";
		break;
	case 11:
		strBulan = "November";
		break;
	case 12:
		strBulan = "Desember";
		break;
	}
	return strBulan;
}

var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);
 
/**
 * keluaran yang diharapkan (pada console)
 *
 * ["0001", "Roman Alamsyah Elsharawy", "Provinsi Bandar Lampung", "21/05/1989", "Pria", "SMA Internasional Metro"]
 * Mei
 * ["1989", "21", "05"]
 * 21-05-1989
 * Roman Alamsyah
 */ 