var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise 
var i = 0

bacaBuku(10000)

function bacaBuku(time) {
	if(i < books.length) {
		readBooksPromise(time, books[i])
		.then(function (times) {
            bacaBuku(times)
        })
        .catch(function (error) {
            console.log("Waktu saya habis");
        });

		i++
	}
}

