//Soal No. 1
function arrayToObject(arr) {
    var now = new Date()
    var thisYear = now.getFullYear() // 2020 (tahun sekarang)
    var age

    for(var i = 0; i < arr.length; i++) {
        if(arr[i][3] == null || arr[i][3] > thisYear) {
            age = "Invalid birth year"
        } else {
            age = thisYear - arr[i][3]
        }

        var person = { 
            firstName: arr[i][0],
            lastName: arr[i][1],
            gender: arr[i][2],
            age: age
        }

        process.stdout.write((i+1)+". "+person.firstName+" "+person.lastName+": ")
        console.log(person);
    }
}
 
// Driver Code
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
arrayToObject(people) 
 
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]
arrayToObject(people2) 
 
// Error case 
arrayToObject([]) // ""

//Soal No. 2 
function shoppingTime(memberId, money) {
    if(memberId != null && memberId != "") {
        if(money >= 50000) {
            var listSale = [
                ["Sepatu brand Stacattu", 1500000],
                ["Baju brand Zoro", 500000],
                ["Baju brand H&N", 250000],
                ["Sweater brand Uniklooh", 175000],
                ["Casing Handphone", 50000]
            ]

            var listPurchased = []
            var cash = money

            for(var i = 0; i < listSale.length; i++) {
                if(cash >= listSale[i][1]) {
                    listPurchased.push(listSale[i][0]) 
                    cash-=listSale[i][1]
                }
            }

            return { 
                memberId: memberId,
                money: money,
                listPurchased: listPurchased,
                changeMoney: cash
            }
        } else return "Mohon maaf, uang tidak cukup"
    } else return "Mohon maaf, toko X hanya berlaku untuk member saja"
}

// TEST CASES
console.log(shoppingTime('1820RzKrnWn08', 2475000));

console.log(shoppingTime('82Ku8Ma742', 170000));

console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

//Soal No. 3
function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var arrObjectPenumpang = []

    for(var i = 0; i < arrPenumpang.length; i++) {
        var rute1, rute2

        for(var j = 0; j < rute.length; j++) {
            if(arrPenumpang[i][1] == rute[j]) {
                rute1 = j
            }
            if(arrPenumpang[i][2] == rute[j]) {
                rute2 = j
            }
        }
        var bayar = (rute2-rute1)*2000

        var penumpang = {
            penumpang: arrPenumpang[i][0],
            naikDari: arrPenumpang[i][1],
            tujuan: arrPenumpang[i][2],
            bayar: bayar
        }

        arrObjectPenumpang.push(penumpang)
    }
    return arrObjectPenumpang
}
 
//TEST CASE
console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
// [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
//   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
 
console.log(naikAngkot([])); //[]