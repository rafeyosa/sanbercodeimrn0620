import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import LoginScreen from './Tugas/Tugas13/LoginScreen';
import AboutScreen from './Tugas/Tugas13/AboutScreen';
import ToDoApp from './Tugas/Tugas14/App';
import SkillScreen from './Tugas/Tugas14/SkillScreen';
import NavigationScreen from './Tugas/Tugas15/index';
import ImplementReactNavigation from './Tugas/TugasNavigation/index';
import Quiz3 from './Tugas/Quiz3/index';
import ApiTest from './src/apitest/index';
import ReduxTest from './src/app';

export default function App() {
  return (
    <ReduxTest />
    /*
    <View style={styles.container}>
      <Text>Open up App.js to start working on your app!</Text>
      <StatusBar style="auto" />
    </View>
    */
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
