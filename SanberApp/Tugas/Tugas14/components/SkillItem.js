import React, { Component } from 'react';
import {Platform, StyleSheet, Text, View, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default class SkillItem extends Component {
    render() {
        let skill = this.props.skill;
        let percentVal = skill.percentageProgress;
        return (
            <View style={styles.container}>
                <View style={styles.imageSkill}>
                    <Image source={{ uri: skill.logoUrl }} style={styles.logo} />
                    <View style={styles.nameSkill}>
                        <Text style={styles.textName}>{skill.skillName}</Text>
                        <Text style={styles.textName}>({skill.level})</Text>
                    </View>
                </View>
                
                <Text style={styles.textCategory}>{skill.categoryName}</Text>
                <View style={styles.progressBar}>
                    <View style={{height:"100%", borderRadius: 20, backgroundColor: "#877EF3", width: percentVal}}>
                        <Text style={styles.percent}>{skill.percentageProgress}</Text>
                    </View>
                </View>
                
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 10,
        borderWidth: 2,
        borderRadius: 5,
        borderColor: "black",
        flexDirection:"column",
        padding: 5,
        backgroundColor: "white"
    },
    logo: {
        height: 50,
        width: 50,
    }, 
    imageSkill: {
        flexDirection:"row",
    },
    nameSkill: {
        flexDirection:"column",
        marginStart: 20
    },
    textName: {
        fontSize: 20,
    },
    textCategory: {
        fontSize: 20,
        color: "rgba(0,0,0,0.7)",
        marginTop: 5,
        marginBottom: 5,
        marginStart: 10,
    },
    progressBar: {
        height:25,
        marginTop: 5,
        marginStart: 10,
        marginEnd: 5,
        marginBottom: 10,
        borderWidth: 2,
        borderRadius: 20,
        borderColor: "black",
    },
    bar: {
        height:"100%",
        borderRadius: 20,
        backgroundColor: "#877EF3",
    },
    percent: {
        fontSize:16,
        textAlign:"right",
        marginEnd:10,
    }
});