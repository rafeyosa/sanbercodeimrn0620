import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, ImageBackground, StatusBar, FlatList} from 'react-native'
import SkillItem from './components/SkillItem';
import data from './skillData.json';


export default class SkillScreen extends Component {

    render(){
        return (
            <ImageBackground source={require('./images/background.png')} style={styles.container}>
                <StatusBar />
                <View style={styles.nameBar}>
                    <Image source={require('./images/rafeyosa.png')} style={styles.image} />
                    <View style={styles.row}>
                        <Text style={styles.name}>Rafeyosa</Text>
                    </View>
                </View>
                <View style={styles.body}>
                    <FlatList
                    data={data.items}
                    renderItem={(skill)=> <SkillItem skill={skill.item} />}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={()=><View style={{height:5}}/>}
                    />
                </View>
            </ImageBackground>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    image: {
        width: 40,
        height: 40,
    },
    nameBar: {
        height: 65,
        backgroundColor: '#8FA0FB',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        marginTop:40,
        marginLeft:"45%"
    },
    name: {
        marginLeft: 10,
        fontSize: 20,
    },
    row: {
        flexDirection: 'row'
    },
    body: {
        marginTop: 40,
        paddingStart: 40,
        paddingEnd: 40,
        flex: 1
    },
});