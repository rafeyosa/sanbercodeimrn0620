import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, StatusBar} from 'react-native'

export default class AboutScreen extends Component {

    render(){
        return (
            <View style={styles.container}>
                <StatusBar />
                <Image source={require('./images/rafeyosa.png')} style={styles.logo}/>
                <Text style={styles.nama}>Rahmat Febri Yoga Saputra</Text>
                <View style={styles.border}>
                    <View style={styles.borderHorizontal}>
                        <Image source={require('./images/logo-facebook.png')} style={styles.minilogo}/>
                        <Text style={styles.porto}>facebook.com/rafeyosa</Text>
                    </View>
                    <View style={styles.borderHorizontal}>
                        <Image source={require('./images/logo-instagram.png')} style={styles.minilogo}/>
                        <Text style={styles.porto}>@rafeyosa</Text>
                    </View>
                    <View style={styles.borderHorizontal}>
                        <Image source={require('./images/logo-whatsapp.png')} style={styles.minilogo}/>
                        <Text style={styles.porto}>+62 852 7466 0910</Text>
                    </View>
                </View>
                <View style={styles.bluebox}>
                    <Text style={styles.portofolio}>Portofolio</Text>
                    <Text style={styles.portofolio}>https://gitlab.com/rafeyosa/sanbercodeimrn0620</Text>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo:{
        marginBottom:40
    },
    nama:{
        fontSize:22,
        textDecorationLine: "underline"
    },
    border: {
        marginTop: 20,
        width: "80%",
        borderWidth: 2,
        borderRadius: 5,
        borderColor: "black",
        flexDirection:"column"
    },
    borderHorizontal: {
        flexDirection:"row"
    },
    minilogo: {
        margin: 10,
    },
    porto: {
        marginTop:12,
        fontSize:20,
    },
    portofolio: {
        fontSize:20,
    },
    bluebox: {
        padding:5,
        marginTop: 20,
        width: "80%",
        borderWidth: 2,
        borderRadius: 5,
        borderColor: "rgba(0,0,0,0.1)",
        backgroundColor: "rgba(120,124,215,0.38)",
        flexDirection:"column",
    },
});