import React, { Component } from 'react'
import { StyleSheet, Text, View, Image, ImageBackground, StatusBar, TextInput, TouchableOpacity} from 'react-native'

export default class LoginScreen extends Component {

    render(){
        return (
            <View style={styles.container}>
                <StatusBar />
                <ImageBackground source={require('./images/background.png')} style={styles.image}>
                    <Image source={require('./images/logo.png')} style={styles.logo}/>
                    
                    <View style={styles.inputView} >
                        <TextInput  
                        style={styles.inputText}
                        placeholder="Email" 
                        placeholderTextColor="#FFFFFF"/>
                    </View>
                    <View style={styles.inputView} >
                        <TextInput  
                        secureTextEntry
                        style={styles.inputText}
                        placeholder="Password" 
                        placeholderTextColor="#FFFFFF"/>
                    </View>
                    <TouchableOpacity style={styles.loginBtn}>
                        <Text style={styles.loginText}>LOGIN</Text>
                    </TouchableOpacity>

                </ImageBackground>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
    },
    image: {
        flex: 1,
        resizeMode: "cover",
        justifyContent: "center",
        alignItems: 'center',
        justifyContent: 'center',
    },
    logo:{
        marginBottom:40
    },
    inputView:{
        width:"80%",
        backgroundColor:"rgba(0,0,0,0.35)",
        borderRadius:25,
        height:50,
        marginBottom:20,
        justifyContent:"center",
        padding:20
    },
    inputText:{
        height:50,
        color:"white"
    },
    loginBtn:{
        width:"50%",
        backgroundColor:"#768BD4",
        borderRadius:25,
        height:50,
        alignItems:"center",
        justifyContent:"center",
        marginTop:40,
        marginBottom:10,
    },
    loginText:{
        color:"black"
    }
});